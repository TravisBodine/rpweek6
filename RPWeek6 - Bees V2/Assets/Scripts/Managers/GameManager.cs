﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameStats stats;

    public QueenBee queenBee;

    public int CurScore = 0;

    public List<GameObject> curEnemies;
    public List<GameObject> curScoreObjects;

    [HideInInspector]
    public MenuManager menuManager;

    void Awake()
    {
 
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    public void HandleWin()
    {
        Time.timeScale = 0;
        menuManager.ChangeMenu("WinScreen");
        Debug.Log("youwin");
    }

    public void HandleLoss()
    {
        Time.timeScale = 0;
        menuManager.ChangeMenu("LoseScreen");
        Debug.Log("youLose");
    }

    public void ResetLevel()
    {
        Time.timeScale = 1;
        CurScore = 0;
    }
}
