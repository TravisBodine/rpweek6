﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="GameStats")]
public class GameStats : ScriptableObject
{
    public float BeeSpawnSpeed = 3f;
    public int MaxBees= 10;
    public int MaxEnemies = 3;
    public float ScoreObjectSpawnSpeed = 2;
    public float EnemySpawnDelay = 1;

    public int WinScore = 100;

    public float EnemySpawnRate = 10;
    public float PickupSpawnRate = 10;

    
}
