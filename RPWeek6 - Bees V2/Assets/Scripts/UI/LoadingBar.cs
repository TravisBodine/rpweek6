﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class LoadingBar : MonoBehaviour
{
    public Image.FillMethod fillMethod;
    

    Image loadingBar;
    // Start is called before the first frame update
    void Start()
    {
        loadingBar = GetComponent<Image>();
        loadingBar.type = Image.Type.Filled;
        loadingBar.fillMethod = fillMethod;
    }

   public void UpdateBar(float curValue, float maxValue)
   {
        if(loadingBar != null)
            loadingBar.fillAmount = curValue / maxValue;
   }
}
