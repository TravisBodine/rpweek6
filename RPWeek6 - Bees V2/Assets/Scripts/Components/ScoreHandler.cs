﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ScoreHandler : MonoBehaviour
{
    public int Score;
    public static event Action OnScoreUpdated = delegate{};

    // Start is called before the first frame update
    void Start()
    {
        OnScoreUpdated += CheckWin;
    }

    public void AddScore()
    {
        GameManager.instance.CurScore += Score;
        OnScoreUpdated();
    }

    static void CheckWin()
    {

        if(GameManager.instance.CurScore >= GameManager.instance.stats.WinScore)
        {
            GameManager.instance.HandleWin();
        }
    }

    private void OnDisable()
    {

    }

}
