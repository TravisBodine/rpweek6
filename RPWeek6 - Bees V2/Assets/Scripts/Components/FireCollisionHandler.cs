﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireCollisionHandler : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("fire interact");
        Iinteractable interact =  collision.GetComponent<Iinteractable>();

        interact?.interact(gameObject);
        //QueenBee player = collision.gameObject.GetComponent<QueenBee>();
        //if (player != null)
        //{
        //    player.TakeDamage();
        //}
    }
}
