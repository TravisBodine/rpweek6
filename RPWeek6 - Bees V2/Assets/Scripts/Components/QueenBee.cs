﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QueenBee : MonoBehaviour,Iinteractable
{

    public GameObject BeePrefab;

    public List<Bee> Bees = new List<Bee>();

    public float iTime;

    float timer  = 0;

    public int flowersPerBee;
    public int progressToNextBee = 0;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.queenBee = this;

        InputHandler input = GetComponent<InputHandler>();
        if(input != null)
        {
            input.OnFireButtonPressed += CheckForInteractable;
        }
           
        SetupIntialBees();
    }

    void SetupIntialBees()
    {
        Bees.AddRange(GetComponentsInChildren<Bee>());

        foreach(Bee b in Bees)
        {
            b.SwapState(Bee.BeeState.swarm);
        }
    }

    void CheckForInteractable()
    {
        RaycastHit2D[] hits = Physics2D.RaycastAll(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        foreach(RaycastHit2D hit in hits)
        {
            if (hit.collider != null)
            {
                Iinteractable interact = hit.collider.gameObject.GetComponent<Iinteractable>();
                if (interact != null && interact != GetComponent<Iinteractable>())
                {
                    if (Bees.Count > 0)
                    {
                        Debug.Log("wtf");
                        interact.interact(gameObject);
                        //Bee b = Bees[0];
                        //Bees.Remove(b);
                        //b.FlyHome();
                    }
                }
            }
        }
      
    }

    public void interact(GameObject obj)
    {
        Debug.Log("playerinteract");
        FireCollisionHandler enemy = obj.GetComponent<FireCollisionHandler>();
        if(enemy != null)
        {
            if(timer < Time.time)
            {
                TakeDamage();
                timer= iTime + Time.time;
            }
           
        }  
    }

    public void TakeDamage()
    {

        if(Bees.Count > 0)
        {
            killBee();
        }
        if(Bees.Count <= 0)
        {
            GameManager.instance.HandleLoss();
        }
    }

    public void SpawnBee()
    {
        progressToNextBee++;

        if (progressToNextBee >= flowersPerBee)
        {
            GameObject bee = Instantiate(BeePrefab, transform.position, Quaternion.identity);
            bee.transform.parent = GameManager.instance.queenBee.gameObject.transform;

            Bee b = bee.GetComponent<Bee>();
            GameManager.instance.queenBee.Bees.Add(b);
            b.SwapState(Bee.BeeState.swarm);

            progressToNextBee = 0;
        }
      
      
    }

    public void killBee()
    {
        Bee b = Bees[0];
        Bees.Remove(b);

        b.die();
    }

    private void OnDisable()
    {
        GetComponent<InputHandler>().OnFireButtonPressed -= CheckForInteractable;
    }

}
