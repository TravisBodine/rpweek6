﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bee : MonoBehaviour
{
    public float ReturnSpeed = 5;
    Animator an;

    public enum BeeState {idle,swarm}
    public BeeState curState;
    // Start is called before the first frame update
    void Start()
    {
        an = GetComponentInChildren<Animator>();

        transform.rotation = Quaternion.Euler(0, 0, UnityEngine.Random.Range(0, 360));
    }

    public void SwapState(BeeState state)
    {

        curState = state;

        if(an == null)
            an = GetComponentInChildren<Animator>();

        switch (curState)
        {
            case BeeState.idle:
                an.SetBool("Swarming", false);
                break;
            case BeeState.swarm:
                an.SetBool("Swarming", true);
                break;
            default:
                break;
        }
    }

    public void FlyHome()
    {
        SwapState(BeeState.idle);
        transform.parent = null;
        StartCoroutine(Return());
    }

    IEnumerator Return()
    {

        float dist = 10;
        while (dist >= .5)
        {
            Vector3 vectorToTarget = Vector3.zero - transform.position ;

            float angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x);
            transform.rotation = Quaternion.Euler(0,0,angle* Mathf.Rad2Deg - 90);
            transform.Translate(Vector3.up * ReturnSpeed * Time.deltaTime);

            dist = Vector3.Distance(transform.position, Vector3.zero);
            yield return null;
        }

        Destroy(gameObject);
    }

    internal void die()
    {
        Destroy(gameObject);
    }
}
